package com.ivan.imds;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZMQ;

import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ZmqMessageBus {
    private Logger logger = LoggerFactory.getLogger(ZmqMessageBus.class);
    private Queue<String> outputMessageQueue = new ConcurrentLinkedQueue<>();
    private Queue<String> inputMessageQueue = new ConcurrentLinkedQueue<>();
    private ZMQ.Socket serverSocket;
    private ZMQ.Socket clientSocket;

    public ZmqMessageBus(String serverAddress, List<String> clientAddresses) {
        ZMQ.Context context = ZMQ.context(1);
        serverSocket = context.socket(ZMQ.PUB);
        serverSocket.bind(serverAddress);

        clientSocket = context.socket(ZMQ.SUB);
        clientSocket.subscribe("".getBytes());
        for(String clientAddress : clientAddresses) {
            clientSocket.connect(clientAddress);
        }
    }

    public void start() {
        new Thread(() -> {
            while (true) {
                if(!outputMessageQueue.isEmpty()) {
                    for(String message : outputMessageQueue) {
                        serverSocket.send(message);
                        outputMessageQueue.remove(message);
                    }
                }
            }
        }).start();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(() -> {
            while (true) {
                String received = clientSocket.recvStr();
                if(received != null) {
                    inputMessageQueue.add(received);
                }
            }
        }).start();
    }



    public void pushMessage(String message) {
        this.outputMessageQueue.add(message);
    }

    public Optional<String> popMessage() {
        if(!this.inputMessageQueue.isEmpty()) {
            return Optional.of(this.inputMessageQueue.remove());
        }

        return Optional.empty();
    }

    public Queue<String> getOutputMessageQueue() {
        return outputMessageQueue;
    }

    public Queue<String> getInputMessageQueue() {
        return inputMessageQueue;
    }
}
