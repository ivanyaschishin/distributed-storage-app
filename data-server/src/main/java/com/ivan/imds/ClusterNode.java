package com.ivan.imds;


import java.util.List;
import java.util.stream.Collectors;

public class ClusterNode {

    private DataServer dataServer;
    private ZmqMessageBus messageBus;
    private String name;

    public ClusterNode(String name, ClusterNodeConfig nodeConfig, List<ClusterNodeConfig> clusterNodes) throws Exception {
        messageBus = new ZmqMessageBus("tcp://*:" + nodeConfig.getSyncPort(), clusterNodes.stream()
                .map(clusterNodeConfig -> "tcp://" + clusterNodeConfig.getHostname() + ":" + clusterNodeConfig.getSyncPort())
                .collect(Collectors.toList()));

        dataServer = new DataServer(nodeConfig.getDataPort(), messageBus);
        this.name = name;
    }

    public void start() {
        messageBus.start();
        dataServer.start();
    }

    public String getName() {
        return name;
    }
}
