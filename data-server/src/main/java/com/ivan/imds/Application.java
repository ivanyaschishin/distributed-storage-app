package com.ivan.imds;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws Exception {

        ClusterConfigHolder configHolder = new ClusterConfigLoader().load();

        if(args.length >= 1 && !args[0].isEmpty()) {
            String nodeName = args[0];
            ClusterNode clusterNode = new ClusterNode(nodeName, configHolder.getConfig(nodeName), configHolder.getAdjacent(nodeName));
            clusterNode.start();

        } else {
            List<ClusterNode> nodes = new ArrayList<>();

            for(String nodeName : configHolder.getNodes()) {
                ClusterNode clusterNode = new ClusterNode(nodeName, configHolder.getConfig(nodeName), configHolder.getAdjacent(nodeName));
                nodes.add(clusterNode);
            }

            nodes.forEach(ClusterNode::start);
        }


    }
}
