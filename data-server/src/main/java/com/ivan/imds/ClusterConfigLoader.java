package com.ivan.imds;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class ClusterConfigLoader {
    private static final String CONFIG_FILE_NAME = "cluster.properties";
    private final Logger logger = LoggerFactory.getLogger(ClusterConfigLoader.class);

    public ClusterConfigHolder load() {
        try {
            ClusterConfigHolder holder = new ClusterConfigHolder();
            ((List<String>) IOUtils.readLines(this.getClass().getClassLoader().getResourceAsStream(CONFIG_FILE_NAME))).stream()
                    .map(ClusterNodeConfig::fromString)
                    .forEach(config -> holder.setConfig(config.getNodeName(), config));

            return holder;
        } catch (IOException e) {
            logger.error("Error while loading configs: ", e);
            throw new RuntimeException(e);
        }
    }
}
