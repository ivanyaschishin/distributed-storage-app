package com.ivan.imds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ClusterConfigHolder {

    private Map<String, ClusterNodeConfig> nodeConfigMap = new HashMap<>();

    public ClusterNodeConfig getConfig(String nodeName) {
        return nodeConfigMap.get(nodeName);
    }

    public void setConfig(String nodeName, ClusterNodeConfig config) {
        nodeConfigMap.put(nodeName, config);
    }

    public List<ClusterNodeConfig> getAdjacent(String nodeName) {
        return nodeConfigMap.values().stream()
                .filter(clusterNodeConfig -> !clusterNodeConfig.getNodeName().equals(nodeName))
                .collect(Collectors.toList());
    }

    public Set<String> getNodes() {
        return nodeConfigMap.keySet();
    }
}
