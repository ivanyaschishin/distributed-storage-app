package com.ivan.imds;


import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class DataServer {
    private Map<String, String> data = new ConcurrentHashMap<>();
    private ServerSocket socket;
    private Logger logger = LoggerFactory.getLogger(DataServer.class);
    private ZmqMessageBus messageBus;

    public void writeData(String key, String value) {
        data.put(key, value);
        messageBus.pushMessage(key + "=" + value);
    }

    public String readData(String key) {
        return data.get(key);
    }

    public DataServer(int dataPort, ZmqMessageBus messageBus) throws Exception {
        socket = new ServerSocket(dataPort);
        this.messageBus = messageBus;
    }

    public void start() {
        logger.info("Starting server");
        new Thread(() -> {
            while (true) {
                try {
                    logger.info("Waiting for next request");
                    Socket connection = socket.accept();
                    logger.info("Processing request");
                    String value = new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();
                    String[] strings = value.split(" ");

                    switch (strings[0]) {
                        case "GET":
                            IOUtils.write(readData(strings[1]), connection.getOutputStream());
                            break;
                        case "SET":
                            writeData(strings[1], strings[2]);
                            IOUtils.write(readData(strings[1]), connection.getOutputStream());
                            break;
                        default:
                            IOUtils.write("Invalid request", connection.getOutputStream());
                            break;
                    }

                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(() -> {
            while (true) {
                try {
                    Optional<String> message = messageBus.popMessage();
                    if(message.isPresent()) {
                        String command = message.get();
                        String[] strings = command.split("=");
                        this.data.put(strings[0], strings[1]);
                        logger.info("Synced field: " + strings[0] + " with value: " + strings[1]);
                    }
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    logger.info("Synchronization thread interrupted ", e);
                }
            }
        }).start();
    }
}
