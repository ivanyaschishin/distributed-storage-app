package com.ivan.imds;


public class ClusterNodeConfig {
    private String hostname;
    private int dataPort;
    private int syncPort;
    private String nodeName;

    public static ClusterNodeConfig fromString(String configString) {
        String[] nodeConfig = configString.split("=");
        String[] nodeConfigValues = nodeConfig[1].split(":");
        return new ClusterNodeConfig(nodeConfig[0], nodeConfigValues[0], Integer.parseInt(nodeConfigValues[1]), Integer.parseInt(nodeConfigValues[2]));
    }

    public ClusterNodeConfig(String nodeName, String hostname, int dataPort, int syncPort) {
        this.nodeName = nodeName;
        this.hostname = hostname;
        this.dataPort = dataPort;
        this.syncPort = syncPort;
    }

    public String getHostname() {
        return hostname;
    }

    public int getDataPort() {
        return dataPort;
    }

    public int getSyncPort() {
        return syncPort;
    }

    public String getNodeName() {
        return nodeName;
    }
}
